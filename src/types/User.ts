export default interface User {
  id?: number;

  login: string;

  name: string;

  password: string;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
